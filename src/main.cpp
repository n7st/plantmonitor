#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <DHT.h>
#include <Wire.h>
#include <LiquidCrystal.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>

// Copy include/config.h.example to include/config.h and add your own
// configuration
#include "config.h"

#define DHTPIN D5
#define DHTTYPE DHT11

WiFiClient espClient;

DHT          dht(DHTPIN, DHTTYPE);
PubSubClient mqtt(espClient);

/**
 * Set up a connection to the MQTT server.
 * 
 * @return void
 */
void reconnectMQTT() {
  int i = 0;

  while (!mqtt.connected()) {
    String clientId = "test";

    if (!mqtt.connect(clientId.c_str())) {
      Serial.printf("MQTT connection attempt %d\n", ++i);
      delay(MQTT_RECONNECTION_DELAY);
    }
  }
}

/**
 * Set up a connection to the WiFi network.
 *
 * @return void
 */
void reconnectWiFi() {
  int i = 0;

  while (WiFi.status() != WL_CONNECTED) {
    delay(WIFI_RECONNECTION_DELAY);
    Serial.printf("WiFi connection attempt %d\n", ++i);
  }
}

/**
 * Runs once at startup time.
 * 
 * @return void
 */
void setup() {
  Serial.begin(9600);
  delay(10);

  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.printf("Connecting to %s...\n", WIFI_SSID);
  reconnectWiFi();

  mqtt.setServer(MQTT_HOST, MQTT_PORT);
  reconnectMQTT();

  Serial.println(F("Connection to WiFi established"));
  Serial.printf("IP address: %s\n", WiFi.localIP().toString().c_str());

  dht.begin();
}

/**
 * Output the humidity, temperature and heat index in celcius.
 * 
 * @param float h humidity
 * @param float t temperature (celcius)
 * @param float hic heat index (celcius)
 * @return void
 */
void output(float h, float t, float hic) {
  Serial.printf("Humidity: %2.f%% | Temperature: %.2f°c | Heat index: %.2f°c\n", h, t, hic);

  // TODO: write to LCD

  String topic("/");
  topic += MQTT_CLIENT;
  topic += "/";
  topic += MQTT_TOPIC;

  // Allocate 101 bytes of RAM (ESP8266)
  StaticJsonDocument<101> doc;

  doc["humidity"]    = h;
  doc["temperature"] = t;
  doc["heat_index"]  = hic;

  char buffer[512];
  size_t n = serializeJson(doc, buffer);

  Serial.println(buffer);

  if (!mqtt.connected()) {
    reconnectMQTT();
  }

  mqtt.publish("somewhere", buffer, n);
}

/**
 * Read from the DHT11 sensor, getting the temperature, humidity and heat index.
 *
 * @return []float
 */
float* getDHTReadings() {
  static float readings[3] = { -999.99, -999.99, -999.99 };

  //float h = dht.readHumidity();
  //float t = dht.readTemperature();
  float h = 20.30;
  float t = 22.55;

  if (!isnan(h) && !isnan(t)) {
    readings[0] = h;
    readings[1] = t;
    readings[2] = dht.computeHeatIndex(t, h, false);
  }

  return readings;
}

/**
 * The main program loop. The delay happens early so the sensor isn't inundated
 * with requests.
 * 
 * @return void
 */
void loop() {
  delay(2000);

  float *dhtReadings = getDHTReadings();

  if (dhtReadings[0] == -999.99) {
    Serial.println(F("Failed to read from DHT sensor"));

    return;
  }

  output(dhtReadings[0], dhtReadings[1], dhtReadings[2]);
}