# PlantMonitor

Firmware for monitoring a plant grow station using a ESP8266 board. Publishes
messages about temperature and humidity to a MQTT server.

_TODO: Water level monitoring, light level monitoring._

## Hardware requirements

* ESP8266 board (the device connects to a WiFi network)
* DHT22 temperature and humidity sensor
* Optionally, a LCD 1602 module (disabled in configuration by default)

## Installation

First, copy `include/config.h.example` to `include/config.h` and edit it with
your own network's values.

### Using PlatformIO

Load the code onto the ESP8266:

```
% sudo platformio run --target upload
```

Monitor serial output:

```
% sudo platformio device monitor --port <PORT HERE>
```

Where `<PORT HERE>` is the device to monitor (for example `/dev/ttyUSB0`).

## License

See [LICENSE](./LICENSE).